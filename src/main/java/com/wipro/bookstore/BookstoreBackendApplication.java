package com.wipro.bookstore;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.wipro.bookstore.model.User;
import com.wipro.bookstore.repository.UserRepository;

@SpringBootApplication
public class BookstoreBackendApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(BookstoreBackendApplication.class, args);
	}
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public void run(String... args) throws Exception {
		this.userRepository.save(new User("Ramesh_Singh", "Ramesh Singh", "1212451232", "akash@gmail.com", "akash12345"));
		this.userRepository.save(new User("Akash_Singh", "Akash Singh", "1256891232", "akashsingh@gmail.com", "akashsingh"));
		this.userRepository.save(new User("Gaurav_Singh", "Gaurav Singh", "1212456852", "gaurav@gmail.com", "gaurav12345"));
		this.userRepository.save(new User("Aakash_Verma", "Aakash Verma", "1758651232", "akash123@gmail.com", "akashverma"));
	}

}
